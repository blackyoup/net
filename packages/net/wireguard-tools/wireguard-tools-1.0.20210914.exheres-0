# Copyright 2019 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service

SUMMARY="Required tools for WireGuard, such as wg(8) and wg-quick(8)"
HOMEPAGE="https://www.wireguard.com/"
DOWNLOADS="https://git.zx2c4.com/${PN}/snapshot/${PNV}.tar.xz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS="examples
    ( providers: iptables nftables ) [[
        *description = [ Firewall rules provider ]
        number-selected = at-least-one
    ]]"

DEPENDENCIES="
    run:
        providers:iptables? ( net-firewall/iptables )
        providers:nftables? ( net-firewall/nftables )
        !net-misc/wireguard[<0.0.20191226] [[
            description = [ File collisions ]
            resolution = upgrade-blocked-before
        ]]

"

# Needs the kernel module and potentially messes up your network connection
RESTRICT="test"

MAKE_PARAMS=(
    BINDIR=/usr/$(exhost --target)/bin
    LIBDIR=/usr/$(exhost --target)/lib
    PLATFORM=linux
    RUNSTATEDIR=/run
    SYSTEMDUNITDIR=${SYSTEMDSYSTEMUNITDIR}
    V=1
    WITH_BASHCOMPLETION=yes
    WITH_WGQUICK=yes
    WITH_SYSTEMDUNITS=yes
)

DEFAULT_SRC_COMPILE_PARAMS=( "${MAKE_PARAMS[@]}" )
DEFAULT_SRC_INSTALL_PARAMS=( "${MAKE_PARAMS[@]}" )

src_compile() {
    edo cd src
    default
}

src_install() {
    edo cd src
    default
    keepdir /etc/wireguard
    option examples && dodoc -r ../contrib
}

