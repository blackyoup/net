# Copyright 2011 Brett Witherspoon <spoonb@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A filtering tool for a bridging firewall"
DESCRIPTION="
The ebtables program is a filtering tool for a Linux-based bridging firewall. It enables transparent
filtering of network traffic passing through a Linux bridge. The filtering possibilities are limited
to link layer filtering and some basic filtering on higher network layers. Advanced logging, MAC
DNAT/SNAT and brouter facilities are also included.

The ebtables tool can be combined with the other Linux filtering tools (iptables, ip6tables and
arptables) to make a bridging firewall that is also capable of filtering these higher network
layers. This is enabled through the bridge-netfilter architecture which is a part of the standard
Linux kernel.
"
HOMEPAGE="https://git.netfilter.org/${PN}"
DOWNLOADS="http://ftp.netfilter.org/pub/${PN}/${PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    run:
        dev-lang/perl:* [[ note = [ ebtables-legacy-save ] ]]
        net-firewall/iptables [[ note = [ provides /etc/ethertypes ] ]]
"

src_install() {
    default

    # /etc/ethertypes is provided by iptables
    edo rm -r "${IMAGE}"/etc

    keepdir /var/lib/ebtables
}

