# Copyright 2021-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=influxdata tag=v${PV} ] \
    flag-o-matic

SUMMARY="CLI for managing resources in InfluxDB v2"
HOMEPAGE+=" https://www.influxdata.com/products/influxdb"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# requires a running server
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/go[>=1.21]
"

pkg_setup() {
    filter-ldflags -Wl,-O1 -Wl,--as-needed
}

src_unpack() {
    default

    edo pushd "${WORK}"
    esandbox disable_net
    edo go mod download -x
    esandbox enable_net
    edo popd
}

src_prepare() {
    default

    export GOROOT="/usr/$(exhost --target)/lib/go"
    export GOPATH="${WORKBASE}"/build

    edo mkdir -p "${WORKBASE}"/build/src/github.com/influxdata/influx-cli/v2
    edo ln -s "${WORK}" "${WORKBASE}"/build/src/github.com/influxdata/influx-cli/v2

    edo ln -s "${TEMP}"/go/pkg "${WORKBASE}"/build
}

src_compile() {
    GO111MODULE="auto" edo go build \
        -o bin/influx \
        -ldflags "${LDFLAGS} -X main.version=${PV}" \
        -v \
        "./cmd/influx"
}

src_install() {
    dobin bin/influx

    emagicdocs
}

