# Copyright 2021-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service [ systemd_files=[ usr/lib/influxdb/scripts/influxdb.service ] ]

SUMMARY="Scalable datastore for metrics, events, and real-time analytics"
HOMEPAGE="https://www.influxdata.com/products/${PN}"
DOWNLOADS="
    listed-only:
        platform:amd64? ( https://dl.influxdata.com/influxdb/releases/${PN}$(ever major)_${PV}-1_amd64.deb )
"

LICENCES="MIT"
SLOT="0"
PLATFORMS="-* ~amd64"
MYOPTIONS="
    platform: amd64
"

DEPENDENCIES="
    build+run:
        group/influxdb
        user/influxdb
    suggestion:
        dev-db/influx-cli [[
            description = [ CLI for managing resources in InfluxDB v2 ]
        ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-2.6.0-systemd.patch
)

WORK=${WORKBASE}

src_unpack() {
    default

    edo tar xf data.tar.gz
}

src_test() {
    :
}

src_install() {
    dobin usr/bin/influxd

    insinto /etc/${PN}
    hereins config.yaml <<EOF
reporting-disabled: true
http-bind-address: "127.0.0.1:8086"
EOF

    install_systemd_files

    insinto /etc/logrotate.d
    doins etc/logrotate.d/${PN}

    keepdir /var/{lib,log}/${PN}
    edo chown -R influxdb:influxdb "${IMAGE}"/var/{lib,log}/${PN}
}

