# Copyright 2014 Nicolas Braud-Santoni <nicolas+exherbo@braud-santoni.eu>
# Distributed under the terms of the GNU General Public License v2

require pam

SUMMARY="One-time password components"
DESCRIPTION="
The OATH Toolkit provide components for building one-time password authentication systems, amongst
which shared libraries, command line tools and a PAM module. It supports the event-based HOTP
algorithm (RFC4226) and the time-based TOTP algorithm (RFC6238).
"
HOMEPAGE="https://www.nongnu.org/${PN}/"
DOWNLOADS="https://download.savannah.gnu.org/releases/${PN}/${PNV}.tar.gz"

LICENCES="LGPL-2 GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-doc/gtk-doc
    build+run:
        dev-libs/libxml2:2.0
        dev-libs/xmlsec
        sys-libs/pam
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-pam
    --enable-pskc
    --disable-static
    --disable-xmltest
    --with-pam-dir=$(getpam_mod_dir)
)

src_test() {
    esandbox allow_net --bind "inet:127.0.0.1@80"
    esandbox allow_net --connect "inet:127.0.0.1@80"

    default

    esandbox allow_net --connect "inet:127.0.0.1@80"
    esandbox disallow_net --bind "inet:127.0.0.1@80"
}

