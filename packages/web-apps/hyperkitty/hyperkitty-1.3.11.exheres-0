# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi py-pep517 [ backend=pdm-backend test=pytest ]

SUMMARY="A web interface to access GNU Mailman v3 archives"

HOMEPAGE+=" https://list.org/"
# Previous releases were on pypi, but they are missing there since 1.3.10+
DOWNLOADS="https://gitlab.com/mailman/${PN}/-/archive/${PV}/${PNV}.tar.bz2"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-python/isort[python_abis:*(-)?]
    build+run:
        dev-python/Django[>=4.2&<5.1][python_abis:*(-)?]
        dev-python/django-compressor[>=1.3][python_abis:*(-)?]
        dev-python/django-extensions[>=1.3.7][python_abis:*(-)?]
        dev-python/django-gravatar2[>=1.0.6][python_abis:*(-)?]
        dev-python/django-haystack[>=2.8.0][python_abis:*(-)?]
        dev-python/django-mailman3[>=1.3.13][python_abis:*(-)?]
        dev-python/django-q2[>=1.0.0][python_abis:*(-)?]
        dev-python/djangorestframework[>=3.0.0][python_abis:*(-)?]
        dev-python/flufl-lock[>=4.0][python_abis:*(-)?]
        dev-python/mailmanclient[>=3.3.3][python_abis:*(-)?]
        dev-python/mistune[>=3.0][python_abis:*(-)?]
        dev-python/networkx[>=2.0][python_abis:*(-)?]
        dev-python/python-dateutil[>=2.0][python_abis:*(-)?]
        dev-python/pytz[>=2012][python_abis:*(-)?]
        dev-python/robot-detection[>=0.3][python_abis:*(-)?]
    test:
        dev-python/Whoosh[>=2.5.7][python_abis:*(-)?]
        dev-python/beautifulsoup4[>=4.3.2][python_abis:*(-)?]
        dev-python/elasticsearch[python_abis:*(-)?]
        dev-python/django-debug-toolbar[python_abis:*(-)?]
        dev-python/lxml[python_abis:*(-)?]
        dev-python/pytest-django[python_abis:*(-)?]
"

PYTEST_SKIP=(
    # Apparently this is due to some changes in python
    # https://gitlab.com/mailman/hyperkitty/-/issues/514
    test_cant_write_error
)

PYTEST_PARAMS=(
    --ds=hyperkitty.tests.settings_test
    hyperkitty
)

install_one_multibuild() {
    py-pep517_install_one_multibuild

    # example_project is not a nice thing to install to system site-packages
    # dir and will conflict
    # https://gitlab.com/mailman/hyperkitty/-/issues/279
    edo rm -r "${IMAGE}"$(python_get_sitedir)/example_project
}

