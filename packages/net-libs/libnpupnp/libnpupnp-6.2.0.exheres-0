# Copyright 2021-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require meson

SUMMARY="C++ base UPnP library, derived from Portable UPnP, a.k.a libupnp"
HOMEPAGE="https://www.lesbonscomptes.com/upmpdcli"
DOWNLOADS="${HOMEPAGE}/downloads/${PNV}.tar.gz"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/expat
        net-libs/libmicrohttpd
        net-misc/curl
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dclient=true
    -Ddevice=true
    -Dexpat=enabled
    -Dgena=true
    -Dipv6=true
    -Doptssdp=true
    -Dsoap=true
    -Dssdp=true
    -Dtestmains=false
    -Dtools=true
    -Dunspecified_server=false
    -Dwebserver=true
)

