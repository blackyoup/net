# Copyright 2023-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=paullouisageneau tag=v${PV} ] cmake

SUMMARY="WebRTC network library featuring Data Channels, Media Transport, and WebSockets"
HOMEPAGE+=" https://${PN}.org/"

LICENCES="MPL-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-libs/json
        dev-libs/plog
        virtual/pkg-config
    build+run:
        dev-libs/glib:2
        net-im/libnice
        net-libs/libsrtp
        net-libs/usrsctp
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCAPI_STDCALL:BOOL=FALSE
    -DENABLE_OPENSSL:BOOL=TRUE
    -DNO_EXAMPLES:BOOL=TRUE
    -DNO_MEDIA:BOOL=FALSE
    -DNO_WEBSOCKET:BOOL=FALSE
    -DPREFER_SYSTEM_LIB:BOOL=TRUE
    -DUSE_GNUTLS:BOOL=FALSE
    -DUSE_MBEDTLS:BOOL=FALSE
    -DUSE_NETTLE:BOOL=FALSE
    -DUSE_NICE:BOOL=TRUE
    -DUSE_SYSTEM_JSON:BOOL=TRUE
    -DUSE_SYSTEM_JUICE:BOOL=TRUE
    -DUSE_SYSTEM_PLOG:BOOL=TRUE
    -DUSE_SYSTEM_SRTP:BOOL=TRUE
    -DUSE_SYSTEM_USRSCTP:BOOL=TRUE
    -DWARNINGS_AS_ERRORS:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DNO_TESTS:BOOL=FALSE -DNO_TESTS:BOOL=TRUE'
)

